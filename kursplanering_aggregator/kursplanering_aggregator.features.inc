<?php
/**
 * @file
 * kursplanering_aggregator.features.inc
 */

/**
 * Implements hook_views_api().
 */
function kursplanering_aggregator_views_api() {
  return array("version" => "3.0");
}
