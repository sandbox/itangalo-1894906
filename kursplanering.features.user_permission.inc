<?php
/**
 * @file
 * kursplanering.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function kursplanering_user_default_permissions() {
  $permissions = array();

  // Exported permission: access administration menu.
  $permissions['access administration menu'] = array(
    'name' => 'access administration menu',
    'roles' => array(
      0 => 'resource contributor',
      1 => 'teacher',
    ),
    'module' => 'admin_menu',
  );

  // Exported permission: access user profiles.
  $permissions['access user profiles'] = array(
    'name' => 'access user profiles',
    'roles' => array(
      0 => 'resource contributor',
      1 => 'teacher',
    ),
    'module' => 'user',
  );

  // Exported permission: change own username.
  $permissions['change own username'] = array(
    'name' => 'change own username',
    'roles' => array(
      0 => 'resource contributor',
      1 => 'teacher',
    ),
    'module' => 'user',
  );

  // Exported permission: create cp_offering content.
  $permissions['create cp_offering content'] = array(
    'name' => 'create cp_offering content',
    'roles' => array(
      0 => 'teacher',
    ),
    'module' => 'node',
  );

  // Exported permission: create cp_outline content.
  $permissions['create cp_outline content'] = array(
    'name' => 'create cp_outline content',
    'roles' => array(
      0 => 'teacher',
    ),
    'module' => 'node',
  );

  // Exported permission: create cp_resource content.
  $permissions['create cp_resource content'] = array(
    'name' => 'create cp_resource content',
    'roles' => array(
      0 => 'resource contributor',
      1 => 'teacher',
    ),
    'module' => 'node',
  );

  // Exported permission: create cp_section content.
  $permissions['create cp_section content'] = array(
    'name' => 'create cp_section content',
    'roles' => array(
      0 => 'teacher',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own cp_offering content.
  $permissions['delete own cp_offering content'] = array(
    'name' => 'delete own cp_offering content',
    'roles' => array(
      0 => 'teacher',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own cp_outline content.
  $permissions['delete own cp_outline content'] = array(
    'name' => 'delete own cp_outline content',
    'roles' => array(
      0 => 'teacher',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own cp_resource content.
  $permissions['delete own cp_resource content'] = array(
    'name' => 'delete own cp_resource content',
    'roles' => array(
      0 => 'resource contributor',
      1 => 'teacher',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own cp_section content.
  $permissions['delete own cp_section content'] = array(
    'name' => 'delete own cp_section content',
    'roles' => array(
      0 => 'teacher',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own cp_offering content.
  $permissions['edit own cp_offering content'] = array(
    'name' => 'edit own cp_offering content',
    'roles' => array(
      0 => 'teacher',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own cp_outline content.
  $permissions['edit own cp_outline content'] = array(
    'name' => 'edit own cp_outline content',
    'roles' => array(
      0 => 'teacher',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own cp_resource content.
  $permissions['edit own cp_resource content'] = array(
    'name' => 'edit own cp_resource content',
    'roles' => array(
      0 => 'resource contributor',
      1 => 'teacher',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own cp_section content.
  $permissions['edit own cp_section content'] = array(
    'name' => 'edit own cp_section content',
    'roles' => array(
      0 => 'teacher',
    ),
    'module' => 'node',
  );

  // Exported permission: show format selection for comment.
  $permissions['show format selection for comment'] = array(
    'name' => 'show format selection for comment',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'better_formats',
  );

  // Exported permission: show format selection for field_collection_item.
  $permissions['show format selection for field_collection_item'] = array(
    'name' => 'show format selection for field_collection_item',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'better_formats',
  );

  // Exported permission: show format selection for node.
  $permissions['show format selection for node'] = array(
    'name' => 'show format selection for node',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'better_formats',
  );

  // Exported permission: show format selection for rules_config.
  $permissions['show format selection for rules_config'] = array(
    'name' => 'show format selection for rules_config',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'better_formats',
  );

  // Exported permission: show format selection for taxonomy_term.
  $permissions['show format selection for taxonomy_term'] = array(
    'name' => 'show format selection for taxonomy_term',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'better_formats',
  );

  // Exported permission: show format selection for user.
  $permissions['show format selection for user'] = array(
    'name' => 'show format selection for user',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'better_formats',
  );

  // Exported permission: show format tips.
  $permissions['show format tips'] = array(
    'name' => 'show format tips',
    'roles' => array(),
    'module' => 'better_formats',
  );

  // Exported permission: show more format tips link.
  $permissions['show more format tips link'] = array(
    'name' => 'show more format tips link',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'better_formats',
  );

  // Exported permission: unique_field_perm_bypass.
  $permissions['unique_field_perm_bypass'] = array(
    'name' => 'unique_field_perm_bypass',
    'roles' => array(
      0 => 'resource contributor',
      1 => 'teacher',
    ),
    'module' => 'unique_field',
  );

  // Exported permission: use Rules component rules_cp_copy_lesson_to_weeks.
  $permissions['use Rules component rules_cp_copy_lesson_to_weeks'] = array(
    'name' => 'use Rules component rules_cp_copy_lesson_to_weeks',
    'roles' => array(
      0 => 'administrator',
      1 => 'teacher',
    ),
    'module' => 'rules',
  );

  // Exported permission: use Rules component rules_cp_lesson_repeat.
  $permissions['use Rules component rules_cp_lesson_repeat'] = array(
    'name' => 'use Rules component rules_cp_lesson_repeat',
    'roles' => array(
      0 => 'administrator',
      1 => 'teacher',
    ),
    'module' => 'rules',
  );

  // Exported permission: view own unpublished content.
  $permissions['view own unpublished content'] = array(
    'name' => 'view own unpublished content',
    'roles' => array(
      0 => 'resource contributor',
      1 => 'teacher',
    ),
    'module' => 'node',
  );

  // Exported permission: view the administration theme.
  $permissions['view the administration theme'] = array(
    'name' => 'view the administration theme',
    'roles' => array(
      0 => 'resource contributor',
      1 => 'teacher',
    ),
    'module' => 'system',
  );

  return $permissions;
}
