<?php
/**
 * @file
 * kursplanering.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function kursplanering_user_default_roles() {
  $roles = array();

  // Exported role: resource contributor.
  $roles['resource contributor'] = array(
    'name' => 'resource contributor',
    'weight' => '4',
  );

  // Exported role: teacher.
  $roles['teacher'] = array(
    'name' => 'teacher',
    'weight' => '5',
  );

  return $roles;
}
